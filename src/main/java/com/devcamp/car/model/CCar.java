package com.devcamp.car.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;
@Entity
@Table(name="car")
public class CCar {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "car_code", unique = true)
	private String carCode;
	
	@Column(name = "car_name", unique = true)
	private String carName;
	
	@OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
    @JsonManagedReference
	private Set<CCarType> carType;
	
	public CCar(String carCode, String carName) {
		super();
		this.carCode = carCode;
		this.carName = carName;
	}

	public CCar() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCarCode() {
		return carCode;
	}

	public void setCarCode(String carCode) {
		this.carCode = carCode;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public Set<CCarType> getCarType() {
		return carType;
	}

	public void setCarType(Set<CCarType> carType) {
		this.carType = carType;
	}

}
